# Transkingdom network analysis provides insight into host-microbiome interactions in Atlantic salmon
-- Code for reproducing results

Directions for running scripts:

Stage 0 and 1 has been pre-run, which means that there is no need to run these.
Output from these scripts are saved as RData-files in the `Data/RData` directory.
Stage 2, 3 and 4 can be run using these RData files.

Rstudio should prompt you to install packages R lacks when the Rmd files are opened.


