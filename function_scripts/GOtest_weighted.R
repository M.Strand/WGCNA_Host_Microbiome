# Find GO enrichment for salmon genes.
# This function takes a vector of in universe genes and a vector of subset genes.
# topGO: Improve GO group scoring using the underlying GO graph topology. Alexa et al. (2006).

# corMEx: a named vector of each gene correlated to the module eigengene
# gene_universe: a vector of all feasible genes
# term: a string, possible terms are; 
# 'MF' for molecular function.
# 'BP' for biological function. 
# 'CC' for cellular component.

# load(file = "data/RData/RNAseqNetworkReady_combined.RData")
# load(file = "data/RData/rna_modules_bicor_4b_mad_max_combined.RData")
# full_expression_matrix <- RNAseqList$all_rna
# MEs <- modules.rna$MEs
# which_module <- 27; term = "BP"; topNodes = 20
# selFun = function(x, corr_threshold = 0.8){x > corr_threshold} 
# Where x is the correlations between a module eigengene and all genes.
# Can be changed to any function, that takes in the correlations as a vector and selects genes based on those correlations.

GOtest_weighted <- function(MEs, which_module = 1, full_expression_matrix, term = "BP", 
                             selFun = function(x, corr_threshold = 0.8){x > corr_threshold},
                             topNodes = 20){
  options(stringsAsFactors = FALSE)
  suppressMessages(library(topGO))           # For GO enrichment
  suppressMessages(library(GO.db))           # Get go terms and structure?
  suppressMessages(library("Ssa.RefSeq.db")) # Salmon database
  suppressMessages(library(reshape2))        # Transforming data into correct format
  
  
  # Correlate every gene to the module eigene gene of your choosen module:
  corMEx <- WGCNA::bicor(MEs[[paste0("ME",which_module)]], full_expression_matrix) # This step is quick.
  names(corMEx) <- colnames(full_expression_matrix) # Set gene names as vector names for each correlation value.
  gene_universe <- colnames(full_expression_matrix) # This is also our gene universe.
  
  # Gene universe is all feasible genes, i.e. all genes that could end up in the subset given a random selection.
  # Practically this means that we only select genes for our gene universe that are used in the creation of modules.
  geneID2GO <- get.GO(gene_universe, TERM = term)
  gene2GOwide <- split(geneID2GO$gene_id, as.factor(geneID2GO$go_id)) # splits to a names list.
  # The list names are GO identifiers. Each GO contains a character vector of genes which are mapped to it.
  # Below in the creation of the topGOdata object, annFUN.GO2genes expects this structure. 
  # There are many annFUN function types depending what information you have. See ?annFUN
  
  # You will have a loss of genes here because not all genes have a GO term associated with it.
  # This means that we are only scoring genes that have GO terms associated with them.
  gene_universe_wGOid <- corMEx[which(names(corMEx) %in% unique(geneID2GO$gene_id))] 
  
  
  # Making the GOdata object ----

  sampleGOdata <- new("topGOdata",                          # creates a new object of class topGOdata
                     description = "Module GO enrichment", # name of session, what ever you want
                     ontology=term,                        # One of 'BP', 'MF' or 'CC'
                     allGenes = gene_universe_wGOid, # gene universe: p-values for all genes OR factor defining in and out group
                     geneSel = selFun,    # A way to select interesting genes
                     nodeSize = 5,        # cutoff: kickout GO nodes that have less than x genes annotated to them 
                     annot = annFUN.GO2genes, # Function defining how to interpret GO to gene mapping
                     GO2genes = gene2GOwide)  # The GO term to gene mapping. Passed on to function
  
  # Preforming the enrichment test ----
  # p-values computed by the runTest function are unadjusted for multiple testing; 
  # While the topGO creators do not advocate against adjusting p-values, they want to note that
  # in many cases adjusting p-values might be misleading with some types of enrichment analysis.
  
  # There are several possible statistical tests and algorithms in topGO.
  # Not all combinations are possible, see topGO vignette.
  # Statistical tests: fisher | ks | t | globaltest | sum
  # Algorithms: classic | elim | weight | weight01 | lea | parentchild
  
  # Here we do two combinations (these new variables are topGOresult objects).
  resultFisher.classic <- runTest(sampleGOdata, algorithm = "classic", statistic = "fisher")
  resultFisher.weight01 <- runTest(sampleGOdata, algorithm = "weight01", statistic = "fisher")
  
  # For the classic method 'each GO category is tested independently.'
  # Weight01 'is a mixture between the elim and the weight algorithms.'
  
  # Analysis of result ----
  # GenTable function gives a table of results.
  # You can also use score(x) to get p-values for each topGOresult object.
  # extract the results "topNodes" number of nodes.
  final <- GenTable(sampleGOdata, # add in the topGOresult (from runTest) one by one below and name them
                    classicFisher = resultFisher.classic,
                    weight01Fisher = resultFisher.weight01,
                    ranksOf = c("classicFisher"), # Add ranks 1,2,3 etc for a one column 
                    orderBy = "weight01Fisher" ,  # Order by any of the test columns, add column as string
                    topNodes = topNodes)
  
  # topGO has a limit for p-value at '1e-30'. Everything less than that is named '< 1e-30',
  # we change these and transform results to numeric values so that values can be plotted.
  final$classicFisher <- sub(pattern = "< 1e-30", replacement = "1e-30", final$classicFisher)
  final$weight01Fisher <- sub(pattern = "< 1e-30", replacement = "1e-30", final$weight01Fisher)
  final$classicFisher <- as.numeric(final$classicFisher)
  final$weight01Fisher <- as.numeric(final$weight01Fisher)

  # Returning the results ----
  # Only genes with GO terms associated with them was used in the GO-enrichment.
  genes_sub <- genesInTerm(sampleGOdata, whichGO = final$GO.ID) # Get genes for each GO term as a list
  genes_melt <- melt(genes_sub) # Transform into a data frame
  colnames(genes_melt) <- c('gene_id', 'GO.ID') # Add new column names
  #
  # Select only genes after threshold, set as 'corr_threshold'
  idx <- which(genes_melt$gene_id %in% names(gene_universe_wGOid[which(selFun(gene_universe_wGOid) == TRUE)])) 
  genes_present <- genes_melt[idx, ] # Subset to genes that passed the threshold
  # 
  # Type of gene information.
  genes_extra <- get.genes(unique(genes_present$gene_id)) # Get information about each gene
  #
  # Add 'correlation of each gene to the eigengene'.
  idx <- which(names(corMEx) %in% unique(genes_present$gene_id))
  genes_cor <- corMEx[idx]
  genes_cor <- data.frame(row.names = NULL, gene_id = names(genes_cor),  gene_correlation = genes_cor)
  genes_extra <- merge(genes_extra, genes_cor, by = 'gene_id')
  #
  genes_full <- merge(genes_present, genes_extra, by = 'gene_id', all.x = TRUE, sort = FALSE ) # Add this info to the data frame
  genes_full <- genes_full[order(genes_full$GO.ID),] # Order by GO.ID
  #
  # Order the gene table by the GO enrichment result
  genes_full <- merge(genes_full, final[,c(1,8)], by ='GO.ID', all.x = TRUE) # add GO
  genes_full <- genes_full[order(genes_full$weight01Fisher),-8] # Order by p-value, -8 removes p-values from gene table.
  rownames(genes_full) <- NULL # Removes uninformative row names.
  
  out <- list('topGO_object' = sampleGOdata,
              'resultFisher.classic' = resultFisher.classic,
              'resultFisher.weight01' = resultFisher.weight01,
              'result' = final,
              'genes' = genes_full
              )
  out
  
}