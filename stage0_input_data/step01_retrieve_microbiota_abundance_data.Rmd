---
title: "Preprocess 16S from Lars Snipen"
author: "Marius Strand"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r message=FALSE, warning=FALSE}
library(tidyverse)
library(dendextend)
```

Function to fill out the taxonomy table with unclassifed_(known higher rank)
```{r}
source(file = "../function_scripts/fill_in_unclass_tax_df.R")
```

Read in the data.
```{r}
OTU.matrix <- read.delim("../data/OTU_table_097.txt")
rownames(OTU.matrix) <- OTU.matrix$OTU
OTU.matrix <- OTU.matrix[,-1]
dim(OTU.matrix)
otu <- OTU.matrix
```


```{r}
OTU_tax <- rownames(OTU.matrix)
data.frame(OTU_tax) %>% 
  mutate(OTU_tax = sub(pattern = "\\*R=-[0-9]{1,}\\* {1,2}", "", OTU_tax)) %>% 
  mutate(OTU_tax = sub(pattern = ";size=[0-9]{1,}", "", OTU_tax)) %>% 
  separate(col = OTU_tax, into = c("otu_name", 
                                   "Domain", 
                                   "Phylum", 
                                   "Class", 
                                   "Order", 
                                   "Family", 
                                   "Genus", 
                                   "Species"), sep = ";") %>% 
  mutate_all(.funs = function(x){sub(pattern = "[a-z]{1,}__", "", x)}) %>% 
  mutate_all(.funs = function(x){ifelse(grepl(pattern = "unknown", x), NA, x)}) %>% 
  separate(otu_name, into = c("crap", "numb"), sep = "_centroid") %>%
  #arrange(as.numeric(numb)) %>% 
  mutate(otu_name = paste0(crap, "_", numb)) %>%
  select(-c("crap", "numb")) %>%
  select(otu_name, everything()) %>% 
  column_to_rownames(var = "otu_name") -> otu_taxonomy
```

Warnings show that the OTU taxonomy is not complete, most for example lack any species classification.

Fill out the OTU taxonomy marking each unclassified cell with unclassified_x. Where x is the last known rank.

```{r}
otu_taxonomy <- add_unclassified(otu_taxonomy)
```


```{r}
rownames(otu) <- rownames(otu_taxonomy)
```

```{r}
otu[1:10,1:5]
```

```{r}
idx.to.remove <- which(colnames(otu) %in% c("Negative.control1","Positive.control1", "Negative.control2", "Positive.control2"))

otu <- otu[,-idx.to.remove]
```


```{r}
colnames(otu) <- sub(pattern = "FRESH", "FW", colnames(otu))
colnames(otu) <- sub(pattern = "SALT", "SW", colnames(otu))
colnames(otu) <- gsub(pattern = "\\.", "_", colnames(otu))
colnames(otu) <- paste0("G_",colnames(otu))
```


```{r}
colnames(otu) %>% 
  as.data.frame() %>% 
  rename(sampleName = ".") %>% 
  separate(col = sampleName, into = c("Tissue", "Water", "Day", "Feed", "Replicate"), sep = "_", remove = F) %>% 
  mutate(Day = sub("D", "", Day)) %>% 
  mutate(Day = as.numeric(Day)) %>% 
  mutate(Replicate = as.numeric(Replicate))-> sample_info.otu
```


```{r}
otu[1:5,1:5]
sample_info.otu[1:5,]
otu_taxonomy[1:5,]
```


```{r}
save(otu, sample_info.otu, otu_taxonomy, file = "../data/RData/otu_wReplicates.RData")
```



