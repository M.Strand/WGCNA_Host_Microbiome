---
title: "Make modules OTU all data CSS"
author: "Marius Strand"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Load in libraries and set settings.

```{r message=FALSE, warning=FALSE}
options(stringsAsFactors = FALSE)
library(magrittr)                  # For pipes %>% 
library(cowplot)                   # For combining plots
library(dendextend)                # Modify dendrograms
library(WGCNA)                     # Weighted gene co-expression network analysis
library(dplyr)                     # Manipulation of data frames
library(ggplot2)
theme_set(theme_classic())
theme_update(plot.title = element_text(hjust = 0.5))
theme_update(plot.title = element_text(face="bold"))
library(metagenomeSeq)
```

## Settings

Document what settings was used or is going to be used:

```{r}
# Set seed as a precaution for reproducibility as some methods are non-deterministic.
set.seed(13118)
Run_analysis <- TRUE # if FALSE it tries to load data instead of running

save_plots <- TRUE
plot_labeling_size = 15
prefix <- "m"

save_TOM <- TRUE
pam_stage <- FALSE    # Partitioning around medoids, tries to put more OTUs into modules where it is not directly clear from the dendrogram cutting.
set_verbose = 1 # How much detail from the WGCNA functions? Higher means more detail.

omics_type = "otu" 

take_average <- F

max_block_size = 10000      #  OTU data is small enough that this is not necessary.

tissue = "G"                #  G for gut, or L for liver

what_samples = "FW_and_SW"  #  FW, SW or FW_and_SW

applied_filter = 0.005      #  filter lowest x percent of total sum,  100 is 100 percent.

parameter_sets <- list(set_1 = list(applied_norm = "TSS", applied_transf = "CLR", assoc_measure = "bicor"),
                       set_2 = list(applied_norm = "CSS", applied_transf = "log2", assoc_measure = "bicor"))

chosen_parameter_set <- parameter_sets$set_2

pcCorrection <- T
if(pcCorrection){
  estimate_n.pc = T
  if(!estimate_n.pc){
    number_of_pcs_to_remove = 1 # Does not matter when pcCorrection is FALSE
  }
}
```


```{r}

save_name <- paste(omics_type,
                    tissue,
                    ifelse(take_average, "avg", "rep"),
                    applied_filter,
                    chosen_parameter_set$applied_norm,
                    chosen_parameter_set$applied_transf,
                    chosen_parameter_set$assoc_measure,
                    ifelse(pcCorrection, paste0("PcC", ifelse(estimate_n.pc, "", number_of_pcs_to_remove)), "noPcC"),
                    max_block_size,
                    ifelse(pam_stage, "pam", "noPam"),
                    what_samples,
                    sep = "_")

paste0("../data/module_data/", save_name, "_modules.RData")

```

# Subset data

Load in data.
```{r}
input_file <- "../data/RData/otu_wReplicates.RData"

(load(file = input_file))
```
```{r}
omics_data <- otu
```

Load in subset of samples that match between omics data sets.
```{r}
(load(file = "../data/RData/samples_to_keep.RData"))
```
There are no liver samples, for 16S.


Input data `omics_data`:
OTUs are rows, and samples are columns.


```{r}
samples_to_keep <- gut_samples_to_keep

if(what_samples == "FW"){
  samples_to_keep <- samples_to_keep[which(grepl(pattern = "FW", samples_to_keep))]
}
if(what_samples == "SW"){
  samples_to_keep <- samples_to_keep[which(grepl(pattern = "SW", samples_to_keep))]
}
# Or no selection

omics_data <- omics_data[, which(colnames(omics_data) %in% samples_to_keep)]
sample_info <- sample_info.otu[which(sample_info.otu$sampleName %in% samples_to_keep),]
all(colnames(omics_data) == sample_info$sampleName)
```

These are all the samples that are used in this analysis:
```{r}
samples_to_keep 
```

# Filtering

Define the prefilter function and plot consequences of filtering threshold on number of zeros.
```{r}
# function to perform pre-filtering, from mixomics tutorial
low.count.removal = function(
  data, # OTU count data frame of size n (sample) x p (OTU)
  percent=0.01 # cutoff chosen
){
  keep.otu = which(colSums(data)*100/(sum(colSums(data))) > percent)
  data.filter = data[,keep.otu]
  return(list(data.filter = data.filter, keep.otu = keep.otu))
}
```

Plot consequence of filtering
```{r}
frac_zero <- c()
all_sum <- c()
num_otu <- c()
seqp <- seq(0,0.3,0.0001)
for(i in seqp){
  result.filter = low.count.removal(t(omics_data), percent=i)
  length(result.filter$keep.otu)
  otu.f = result.filter$data.filter
  frac_zero <- c(frac_zero, sum(otu.f == 0)/ (ncol(otu.f)*nrow(otu.f)))
  all_sum <- c(all_sum, sum(otu.f))
  num_otu <- c(num_otu, ncol(otu.f))
}
names(frac_zero) <- seqp
names(all_sum) <- seqp

# Get all_sum and num_otu to a fraction of the total
mas <- max(all_sum)
mno <- max(num_otu)

all_sum %>% (function(x){x/max(x)}) -> all_sum
num_otu %>% (function(x){x/max(x)}) -> num_otu
```


```{r fig.height=3, fig.width=6}
data.frame(filter = seqp, 
           Percent.zeros = frac_zero*100, 
           Total.abundance = all_sum*100, 
           Number.of.OTUs = num_otu*100) %>% 
  tidyr::gather(key = "Type", value = "percent.of.total", -filter) %>%
  ggplot() + 
  geom_line(mapping = aes(x = filter, y = percent.of.total, col = Type)) +
  geom_vline(xintercept = applied_filter, alpha = 0.5, color = "red", linetype="dashed")+
  ylab("Percent of total") +
  xlab("Filter at x") +
  theme(legend.title = element_blank())

```

```{r include=FALSE}
if(save_plots){
  ggsave(filename = paste0("../images/other/",
                           "Pre_filter_",
                           paste(omics_type, 
                           tissue, ifelse(take_average, "avg", "rep"),
                           applied_filter, what_samples, sep = "_"), 
                           ".pdf"), 
         device = "pdf", 
         height = 3, 
         width = 6)
}
```

See a dramatic fall in the percent of zeros, a dramatic fall in number of OTUs, but almost no fall in the total sum 
of all OTUs.


```{r}
result.filter = low.count.removal(t(omics_data), percent= applied_filter)
omics_data = result.filter$data.filter; length(result.filter$keep.otu)
omics_data <- t(omics_data)
```



```{r}
omics_data[1:5,1:5]
```




# Normalization and transformation


None, TSS or CSS

```{r}
re <- 0
if(chosen_parameter_set$applied_norm == "TSS"){
  # Normalize
  omics_data <- apply(omics_data, MARGIN = 2, function(x){x/sum(x) * 1000})
  omics_data <- propr:::proprCLR(t(omics_data) + 1) # t(), samples as rows, variables as columns
  omics_data <- t(omics_data)
  report <- "Normalized with TSS and transformed with CLR"
  re <- 1
}


if(chosen_parameter_set$applied_norm == "CSS"){
  # Normalization and transformation in one
  data.metagenomeSeq = metagenomeSeq::newMRexperiment(omics_data) # Variables as rows, samples as columns
  p = metagenomeSeq::cumNormStat(data.metagenomeSeq) #default is 0.5
  data.cumnorm = metagenomeSeq::cumNorm(data.metagenomeSeq, p=p)
  omics_data = metagenomeSeq::MRcounts(data.cumnorm, norm=TRUE, log=TRUE) # log here is a +1 shifted log2
  report <- "Normalized with CSS and transformed with log2"
  re <- 1
}

if(re == 0){
  stop("No transformation done")
}
report
```



# Averaging

```{r}
if(take_average){
  omics_data %>%
    t() %>%
    as.data.frame() %>%
    tibble::rownames_to_column(var = "sampleName") %>%
    dplyr::mutate(sampleName = sub("_[0-9]{1,2}", "", sampleName)) %>%
    dplyr::group_by(sampleName) %>%
    dplyr::summarise_all(.funs = mean) %>%
    dplyr::ungroup() %>%
    tibble::column_to_rownames(var = "sampleName") %>% t() -> omics_data
  
    omics_data <- omics_data[, order(colnames(omics_data))]
  
  sample_info %>% 
    dplyr::select(-c(Replicate)) %>% 
    dplyr::mutate(sampleName = sub("_[0-9]{1,2}", "", sampleName)) %>% 
    dplyr::distinct() -> sample_info
  
  sample_info <- sample_info[order(sample_info$sampleName), ]
  
  report <- "Calculated average for each sample group"
  
  if(!all(colnames(omics_data) == sample_info$sampleName)){
    stop("Something went wrong")
  }
} else{
  report <- "Keep all replicates"
}
report
```




Histogram plotting function
```{r}
plot_hist <- 
  function(m.data, wfun = "sd", plot_title = "OTUs"){ 
    if(wfun == "no"){
      m.data %>% 
      c() %>% 
      as.data.frame() %>% 
      ggplot(aes(x = .)) + 
      geom_histogram(bins = 30, color = "#254466", fill = "#5EACFF") + 
      xlab("Abundance") +
      ggtitle(paste("Expression", "of", plot_title))
    } else{
      m.data %>% 
      apply(1,get(wfun)) %>% 
      data.frame() %>% 
      ggplot(aes(x = .)) + 
      geom_histogram(bins = 30, color = "#254466", fill = "#5EACFF") + 
      xlab(paste(wfun, "value")) +
      ggtitle(paste(wfun, "of", plot_title))
    }
}
```


```{r fig.height=5, fig.width=10}
cowplot::plot_grid(plot_hist(omics_data, "sd", plot_title = paste(chosen_parameter_set$applied_transf, "OTUs")),
                   plot_hist(omics_data, "mad", plot_title = paste(chosen_parameter_set$applied_transf, "OTUs")),
                   plot_hist(omics_data, "no", plot_title = paste(chosen_parameter_set$applied_transf, "OTUs")),
                   align = "h",
                   ncol = 3)
```

```{r include=FALSE}
if(save_plots){
  ggsave(filename = paste0("../images/data_histograms/",
                           "Data_histogram_",
                           paste(omics_type, 
                           tissue, ifelse(take_average, "avg", "rep"),
                           applied_filter, 
                           chosen_parameter_set$applied_norm,
                           chosen_parameter_set$applied_transf, what_samples,sep =
                             "_"),".pdf"),
         device = "pdf", 
         height = 5, 
         width = 10)
}
```


```{r}
dim(omics_data) %>% paste(c("OTUs", "Samples"))
```


# PC-correction or not?

```{r}
if(pcCorrection){
  library(sva)
  if(estimate_n.pc){
    
    print(omics_data %>% dim() %>% paste(c("Samples", "OTUs"))) # samples in rows, variables in columns
    mod=matrix(1,nrow=nrow(t(omics_data)),ncol=1)
    colnames(mod)="Intercept"
    
    print(omics_data %>% dim() %>% paste(c("OTUs", "Samples"))) # Variables in rows, sample columns
    n.pc = num.sv(dat = omics_data, mod = mod,  method="be")
  } else{
    n.pc = number_of_pcs_to_remove
  }
  
  print(omics_data %>% dim() %>% paste(c("OTUs", "Samples"))) # Variables in rows, sample columns
  omics_data <- sva_network(omics_data, n.pc = n.pc)
}
```

```{r fig.height=5, fig.width=10}
if(pcCorrection){
  report <- paste0("PC-correction with ", n.pc, " PC(s) removed.")
  
  cowplot::plot_grid(plot_hist(omics_data, "sd"),
                   plot_hist(omics_data, "mad"),
                   plot_hist(omics_data, "no"),
                   align = "h",
                   ncol = 3)
} else{
  report <- "There was no PC-correction performed."
}
report
```



```{r}
save_data_path <- paste0("../data/transformed_data/", save_name, ".RData")

save(omics_data, identity = save_name, file = save_data_path)
```

# Make modules

## Soft Threshold

Find the soft thresholding power beta to which co-expression similarity is raised to calculate adjacency,
based on the criterion of approximate scale-free topology. 



```{r}
omics_data <- omics_data %>% t()

dim(omics_data) %>% paste(c("Samples", "OTUs"))
```


```{r message=FALSE, warning=FALSE}
powers <- c(1:10, seq(12,20,2))

suppressWarnings(sft <- pickSoftThreshold(omics_data, 
                                          powerVector = powers, 
                                          verbose = set_verbose, 
                                          networkType = "signed",
                                          corFn= chosen_parameter_set$assoc_measure))


# Find the soft thresholding power beta to which co-expression similarity is raised to calculate adjacency.
# based on the criterion of approximate scale-free topology.

idx <- min(which((-sign(sft$fitIndices[,3])*sft$fitIndices[,2]) > 0.90))
if(is.infinite(idx)){
  idx <- min(which((-sign(sft$fitIndices[,3])*sft$fitIndices[,2]) > 0.80))
  if(!is.infinite(idx)){
    st <- sft$fitIndices[idx,1]
  } else{
    idx <- which.max(-sign(sft$fitIndices[,3])*sft$fitIndices[,2])
    st <- sft$fitIndices[idx,1]
  }
} else{
  st <- sft$fitIndices[idx,1]
}


# Plot Scale independence measure and Mean connectivity measure

# Scale-free topology fit index as a function of the soft-thresholding power
data.frame(Indices = sft$fitIndices[,1],
           sfApprox = -sign(sft$fitIndices[,3])*sft$fitIndices[,2]) %>% 
  ggplot() + 
  geom_hline(yintercept = 0.9, color = "red", alpha = 0.6) + # corresponds to R^2 cut-off of 0.9
  geom_hline(yintercept = 0.8, color = "red", alpha = 0.2) + # corresponds to R^2 cut-off of 0.8
  geom_line(aes(x = Indices, y = sfApprox), color = "red", alpha = 0.1, size = 2.5) +
  geom_text(mapping = aes(x = Indices, y = sfApprox, label = Indices), color = "red", size = 4) +
  ggtitle("Scale independence") +
  xlab("Soft Threshold (power)") +
  ylab("SF Model Fit,signed R^2") +
  xlim(1,20) +
  ylim(-1,1) +
  geom_segment(aes(x = st, y = 0.25, xend = st, yend = sfApprox[idx]-0.05), 
               arrow = arrow(length = unit(0.2,"cm")), 
               size = 0.5)-> scale_independence_plot 
  
 


# Mean connectivity as a function of the soft-thresholding power

data.frame(Indices = sft$fitIndices[,1],
           meanApprox = sft$fitIndices[,5]) %>% 
  ggplot() + 
  geom_line(aes(x = Indices, y = meanApprox), color = "red", alpha = 0.1, size = 2.5) +
  geom_text(mapping = aes(x = Indices, y = meanApprox, label = Indices), color = "red", size = 4) +
  xlab("Soft Threshold (power)") +
  ylab("Mean Connectivity") +
  geom_segment(aes(x = st-0.4, 
                   y = sft$fitIndices$mean.k.[idx], 
                   xend = 0, 
                   yend = sft$fitIndices$mean.k.[idx]),
               arrow = arrow(length = unit(0.2,"cm")), 
               size = 0.4) +
  ggtitle(paste0("Mean connectivity: ", 
                 round(sft$fitIndices$mean.k.[idx],2))) -> mean_connectivity_plot


cowplot::plot_grid(scale_independence_plot, mean_connectivity_plot, ncol = 2, align = "h", labels = c("A", "B"), label_size = plot_labeling_size) -> si_mc_plot

si_mc_plot
```


```{r}
st
```


## Block-wise network construction and module detection

The function `blockwiseModules` will first pre cluster with fast crude clustering method to cluster OTUs into blocks not exceeding the maximum, blocks may therefore not be fully optimal in the end.

Change the parameters here to better reflect your own data.
```{r message=FALSE, warning=FALSE}
Run_analysis <- TRUE
if(Run_analysis){
  modules.omics <- blockwiseModules(omics_data,
                          power = st, 
                          networkType = "signed", 
                          TOMType = "signed",
                          corType = chosen_parameter_set$assoc_measure,
                          maxPOutliers = 0.05,
                          deepSplit = 4, # Default 2
                          minModuleSize = 2, # 30
                          minCoreKME = 0.5,      # Default 0.5
                          minCoreKMESize = 2,    # Default minModuleSize/3,
                          minKMEtoStay = 0.5,    # Default 0.3
                          reassignThreshold = 0, # Default 1e-6
                          mergeCutHeight = 0.4,  # Default 0.15
                          pamStage = pam_stage, 
                          pamRespectsDendro = TRUE,
                          replaceMissingAdjacencies = TRUE,
                          numericLabels = TRUE,
                          saveTOMs = save_TOM,
                          saveTOMFileBase = paste0("../data/transformed_data/TOMs/", save_name),
                          verbose = set_verbose)
  
  rownames(modules.omics$MEs) <- rownames(omics_data)
  names(modules.omics$colors) <- colnames(omics_data)
  names(modules.omics$unmergedColors) <- colnames(omics_data)
  
  hubs <- chooseTopHubInEachModule(omics_data, modules.omics$colors, omitColors = 0)
  
  #####
  
  stage2results <- list(modules = modules.omics, 
                        hubs = hubs, 
                        input_file = input_file, 
                        output_data = save_data_path,
                        identity = save_name)
  
  save(stage2results,
       file = paste0("../data/module_data/", save_name, "_modules.RData"))
  
} else {
  load(file = paste0("../data/module_data/", save_name, "_modules.RData"))
}

```


# Visualizing module characteristics

## Description

```{r}
# Convert labels to colors for plotting
merged_colors <- labels2colors(stage2results$modules$colors)
```

```{r include=FALSE}
n_modules <- unique(merged_colors) %>% length()

samples_good <- sum(stage2results$modules$goodSamples) == length(stage2results$modules$goodSamples)
OTUs_good <- sum(stage2results$modules$goodGenes) == length(stage2results$modules$goodGenes)

ME_good <- sum(stage2results$modules$MEsOK) == length(stage2results$modules$MEsOK)
```

`r ifelse(samples_good, "All samples are OK.","Not all samples are OK.")`  
`r ifelse(OTUs_good, "All OTUs are OK.","Not all OTUs are OK.")`  

There where `r n_modules` modules found.  
`r ifelse(ME_good, "All module eigenOTUs are OK.","Not all module eigenOTUs are OK.")`  

How many OTUs are there in each module?
```{r fig.height=5, fig.width=5}
table(stage2results$modules$colors) %>% 
  as.data.frame() %>% 
  dplyr::rename(Module = Var1, Size = Freq) %>% 
  dplyr::mutate(Module_color = labels2colors(as.numeric(as.character(Module)))) -> module_size

# Select only largest modules
module_size <- module_size[1:49,]

module_size %>% 
  ggplot(aes(x = Module, y = Size, fill = Module)) +
  geom_col(color =  "#000000") +
  ggtitle("Number of OTUs in each module") +
  theme(legend.position = "none") + 
  scale_fill_manual(values = setNames(module_size$Module_color,module_size$Module)) +
  geom_text(aes(label = Size),vjust = 0.5, hjust = -0.18, size = 3.5) +
  ylim(0, max(module_size$Size)*1.1) +
  theme(plot.margin = margin(2, 2, 2, 2, "pt")) +
  coord_flip()-> module_size_barplot

module_size_barplot

```

```{r eval=TRUE, include=TRUE}
table(stage2results$modules$colors) %>% as.data.frame() -> res
res$`Module color` <- WGCNA::labels2colors(as.numeric(as.character(res$Var1)))
res <- res[, c(1,3,2)]
colnames(res) <- c("Module", "Module color", "Number of OTUs")
res %>% stargazer::stargazer(type = "latex", summary = FALSE, rownames = FALSE)
```



## Dendrogram and module colors

```{r fig.height=5, fig.width=10}
# Plot the dendrogram and the module colors underneath for each block
for(i in seq_along(stage2results$modules$dendrograms)){
  plotDendroAndColors(stage2results$modules$dendrograms[[i]], merged_colors[stage2results$modules$blockGenes[[i]]],
                      "Module colors",
                      dendroLabels = FALSE, hang = 0.03,
                      addGuide = TRUE, guideHang = 0.05,
                      main = paste0("Cluster Dendrogram\n", 
                                    "for block ", 
                                    i,": ",
                                    length(stage2results$modules$blockGenes[[i]]),
                                    " OTUs"))
}
```


```{r include=FALSE}
if(save_plots){
  pdf(file = paste0("../images/Module_dendrograms/", "Module_dendrogram_",save_name, ".pdf" ), height = 4, width = 6)
  for(i in seq_along(stage2results$modules$dendrograms)){
    plotDendroAndColors(stage2results$modules$dendrograms[[i]], merged_colors[stage2results$modules$blockGenes[[i]]],
                        "Module colors",
                        dendroLabels = FALSE, hang = 0.03,
                        addGuide = TRUE, guideHang = 0.05,
                        main = paste0("Cluster Dendrogram\n", 
                                      "for block ", 
                                      i,": ",
                                      length(stage2results$modules$blockGenes[[i]]),
                                      " OTUs"))
  }
  dev.off()
}
```


## Module (Eigengene) correlation

```{r}
MEs <- stage2results$modules$MEs

# Module correlation to other modules
MEs_R <- bicor(MEs, MEs, maxPOutliers = 0.05)

idx.r <- which(rownames(MEs_R) == "ME0")
idx.c <- which(colnames(MEs_R) == "ME0")

MEs_R_noME0 <- MEs_R[-idx.r, -idx.c]
```


```{r fig.height=5, fig.width=10}
MEs_R_noME0[upper.tri(MEs_R_noME0)] %>% 
  as.data.frame() %>% 
  dplyr::rename("correlation" = ".") %>% 
  ggplot(aes(x=correlation)) + 
  geom_histogram(bins = 20) +
  #geom_density() + 
  xlim(-1, 1) +
  ggtitle(paste0(prefix,"ME correlation\n w/o ",prefix ,"ME0")) -> MEs_R_density

pheatmap::pheatmap(MEs_R_noME0, color = colorRampPalette(c("Blue", "White", "Red"))(100),
                   silent = T, 
                   breaks = seq(-1,1,length.out = 101),
                   treeheight_row = 5, 
                   treeheight_col = 5,
                   main = paste0(prefix,"ME correlation heatmap w/o ",prefix ,"ME0"),
                   labels_row = rep("", length(rownames(MEs_R))), # paste0(prefix, rownames(MEs_R)),
                   labels_col = rep("", length(colnames(MEs_R))) # paste0(prefix, colnames(MEs_R)))
                   ) -> MEs_R_Corr

cowplot::plot_grid(MEs_R_density, MEs_R_Corr$gtable, labels = c("D", "E"), label_size = plot_labeling_size, rel_widths = c(0.6, 1)) -> density_eigen

density_eigen
```



```{r message=FALSE, warning=FALSE}
all(rownames(omics_data) == rownames(MEs))
dim(omics_data) %>% paste0(c(" samples", " OTUs"))
kME <- bicor(omics_data, MEs, maxPOutliers = 0.05)
dim(kME) %>% paste0(c(" OTUs", " modules"))
```

Show a plot of the intra modular correlation; How the OTUs within a module correlates to the module eigengene.

```{r}
intra_cor <- c()
for (i in 1:ncol(omics_data)) {
  m <- stage2results$modules$colors[i]
  intra_cor[i] <- kME[i, paste0("ME", m)]
  if(m != 0){
    intra_cor[i] <- kME[i, paste0("ME", m)]
  } else{
    intra_cor[i] <- NA
  }
  
}

idx <- which(is.na(intra_cor))
intra_cor <- intra_cor[-idx]

plot(density(intra_cor), main = "Correlations with module-eigenOTU (within module correlation)\nNo ME0", xlim = c(-1,1))
```  


Show the same thing, but for each module individually, and color by module color.

```{r}
# Corr within modules
corr_within_module <- function(omics_data, modules, module_x = 1){
  idx.omics_data <- which(modules$colors == module_x)
  idx.me <- which(colnames(modules$MEs) == paste0("ME",module_x))
  kME_x <- bicor(omics_data[,idx.omics_data], modules$MEs[,idx.me], maxPOutliers = 0.05)
  kME_x
}

ggplot.list <- list()

modules <- 
  colnames(stage2results$modules$MEs)[colnames(stage2results$modules$MEs) %>% sub("ME", "", .) %>% as.numeric() %>% order()]

for(m in modules[1:49]){
  h <- as.numeric(sub("ME","", m))
  data.frame(x = suppressWarnings(corr_within_module(omics_data = omics_data, modules = stage2results$modules, module_x = h))) %>% 
    ggplot() + 
    #geom_density(aes(x = x), fill = labels2colors(h), color = "black", alpha = 0.5) + 
    geom_histogram(aes(x), fill = labels2colors(h), color = "black", alpha = 0.5, bins = 20) + 
    xlim(-1, 1) +
    xlab("OTU correlation")+
    ggtitle(paste0(prefix,m)) -> da_plot
  
  ggplot.list[[m]] <- da_plot
}

ggplot.list <- ggplot.list[ggplot.list %>% names() %>% sub("ME", "", .) %>% as.numeric() %>% order()]
```

```{r fig.height=20, fig.width=20}
cowplot::plot_grid(plotlist = ggplot.list) -> density_all_plot
density_all_plot
```


# Combine to one plot

```{r fig.height=15, fig.width=12}
cowplot::plot_grid(si_mc_plot , density_eigen, ncol = 1, rel_heights = c(0.8,1)) -> part_1


cowplot::plot_grid(part_1, module_size_barplot, labels = c("", "C"), label_size = plot_labeling_size, rel_widths = c(1,0.5)) -> part_2


cowplot::plot_grid(part_2, density_all_plot, ncol = 1, rel_heights = c(0.8,1), labels = c("", "F"), label_size = plot_labeling_size)
```



```{r}
if(save_plots){
  ggsave(filename = paste0("../images/Module_diagnostics/",
                           "Module_diagnostics_",save_name, ".pdf"), device =
           "pdf", height = 20, width = 15)
}
```


# Hub genes

For each module it is possible to pick a hub gene with the function `chooseTopHubInEachModule`.


```{r}
stage2results$hubs %>% 
  as.data.frame() %>% 
  dplyr::rename("OTU_name" = ".") %>%
  tibble::rownames_to_column(var = "Module") -> hubOTUs



dplyr::left_join(hubOTUs, 
                 (otu_taxonomy %>%
                    tibble::rownames_to_column(var = "OTU_name")), 
                 by = "OTU_name") -> hubOTUs

hubOTUs
```


# Network centrality

```{r}

  degrees <- intramodularConnectivity.fromExpr(omics_data, colors = modules.omics$colors, power = st,
                                             networkType = "signed", distFnc = chosen_parameter_set$assoc_measure)

degrees$OTU_name <- colnames(omics_data)
degrees$Module <- modules.omics$colors
degrees <- degrees[,c(6,5,1:4)]
 

dplyr::left_join(degrees, 
                 (otu_taxonomy %>%
                    tibble::rownames_to_column(var = "OTU_name")), 
                 by = "OTU_name") -> degrees

write.table(degrees, file = paste0("../export/", save_name, "_degrees.txt"), 
            quote = FALSE, sep = "\t", row.names = FALSE)
```

