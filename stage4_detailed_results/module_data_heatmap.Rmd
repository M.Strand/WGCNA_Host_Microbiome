---
title: "Heatmap of proportion OTU abundance "
author: "Marius Strand"
output: html_document
bibliography: ../bibliography.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
set.seed(13118) # Set seed for reproducibility if any of the methods used are non deterministic.
options(stringsAsFactors = F)
library(magrittr)
library(dplyr)
library(tidyr)
library(tibble)
library(ggplot2)
library(cowplot)
```

Need the raw untransformed data for this to make sense.

```{r}
load(file = "../data/module_data/otu_G_rep_0.005_CSS_log2_bicor_noPcC_10000_noPam_FW_and_SW_modules.RData")

load(stage2results$output_data) # To know which samples and otus to keep

# Get the percentage data

load(stage2results$input_file)

otus_to_keep <- omics_data %>% rownames()

samples_to_keep <- omics_data %>% colnames()

otu.raw <- otu[otus_to_keep, samples_to_keep]

otu.raw %>% dim()

percentage <- function(x){x/sum(x)}

otu.raw <- apply(otu.raw, MARGIN = 2, FUN = percentage)
```

```{r}
take_average <- T
if(take_average){
  otu.raw %>% 
  t() %>% 
  data.frame %>% 
  rownames_to_column(var = "sampleName") %>% 
  mutate(sampleName = sub("_[0-9]{1,2}", "", sampleName)) %>% 
  group_by(sampleName) %>% 
  summarise_all(list(mean)) %>% 
  column_to_rownames(var = "sampleName") %>% 
  t() -> otu.raw
}

```


```{r}
stage2results$modules$dendrograms[[1]] %>% as.dendrogram %>% dendextend::hang.dendrogram(., hang = -1) %>% dendextend::as_hclust_fixed(.) -> otu_clusters
```


Control how the OTU dendrogram is made.
```{r}
otu_annotat_module <- data.frame(otu.names =  names(stage2results$modules$colors), Module = stage2results$modules$colors) %>%
  mutate(Module_color = WGCNA::labels2colors(Module)) %>% 
  mutate(Module = paste0("Module ", Module)) %>% 
  mutate(Module = as.factor(Module))

module_col <- otu_annotat_module[,-1] %>% distinct()
color_vector <- setNames(module_col$Module_color, module_col$Module)


annotation_rows_otu <- data.frame(OTU_name = otu_annotat_module$otu.names, Module = otu_annotat_module$Module)


otu_taxonomy %>% 
  rownames_to_column(var = "OTU_name") -> A

annotation_rows_otu <- left_join(annotation_rows_otu, A, by = "OTU_name") %>% 
  mutate(`Core OTU` = ifelse(OTU_name %in% c("OTU_1", "OTU_2", "OTU_5", "OTU_10"), "Yes", "No")) %>% 
  dplyr::select(OTU_name, Module, Phylum, `Core OTU`) %>% column_to_rownames(var = "OTU_name")
```


```{r}
tax_color_vector <- annotation_rows_otu %>% 
  dplyr::select("Phylum") %>% 
  mutate_all(.funs = list(as.character)) %>%  # Remove factor
  unique() %>% 
  mutate(tax_color = colorRampPalette(RColorBrewer::brewer.pal(8, "Accent"))(nrow(.)))

tax_color_vector <- setNames(as.character(tax_color_vector$tax_color), tax_color_vector$Phylum)
```


```{r}
annotation_colors <- list(
  Feed = c(`FO` = "#F08A46", `VO` = "#8EB470", `FOVO` = "#B7CFA4", `VOFO` = "#F6B890"),
  Water = c(`FW` = "#1D88AF", `SW` = "#12556D"),
  Module = color_vector,
  Day = gray.colors(7, start = 0.9, end = 0.6),
  `Core OTU` = c(`Yes` = "red", `No` = "#F5F5F5"),
  Phylum = tax_color_vector
)
```


```{r}
otu.raw %>% t() -> abundata

abundata[1:5,1:5]
```

```{r}
abundata %>% 
  as.data.frame() %>% 
  tibble::rownames_to_column(var = "Sample_Name") %>% 
  separate(col = Sample_Name, into = c("Tissue","Water", "Day", "Feed", "Replicate"), sep = "_", remove = F, fill = "warn") %>% 
  mutate(Feed = factor(Feed, levels = c("FO", "VOFO", "FOVO", "VO"))) %>% 
  mutate(Water = factor(Water, levels = c("FW", "SW"))) %>% 
  mutate(Day = as.integer(sub("D", "", Day))) %>% 
  arrange(Feed, Water, Day) %>% 
  tibble::column_to_rownames(var = "Sample_Name") %>% 
  dplyr::select(starts_with("OTU")) -> otu.raw.prop_reordered
```


```{r}
otu.raw.prop_reordered %>%
  rownames() %>%
  as.data.frame() %>% 
  dplyr::rename(., "Sample_Name" = ".") %>%
  separate(col = Sample_Name, into = c("Tissue", "Water", "Day", "Feed", "Replicate"), sep = "_", remove = F, fill = "warn") %>% 
  dplyr::select(Sample_Name, Water, Day, Feed) %>%
  mutate(Feed = factor(Feed, levels = c("FO", "VOFO", "FOVO", "VO"))) %>%
  mutate(Water = factor(Water, levels = c("FW", "SW"))) %>%
  mutate(Day = as.integer(sub("D", "", Day))) %>%
  tibble::column_to_rownames(var = "Sample_Name") -> annotation_columns
```



```{r fig.height=12, fig.width=12}
pheatmap::pheatmap(t(otu.raw.prop_reordered), 
                   cluster_cols = T,
                   clustering_method = "ward.D2",
                   cluster_rows = otu_clusters,
                   breaks = c(-0.0001, 0, 0.0001, 0.001, 0.005, seq(0.01,1,length.out = 10)),
                   color = c("#F5F5F5", colorRampPalette(colors = c("#ffc7b8","#3f0000"), bias = 1)(12)), 
                   annotation_row = annotation_rows_otu, 
                   annotation_col = annotation_columns,
                   annotation_colors = annotation_colors,
                   show_rownames = F, 
                   show_colnames = F,
                   fontsize = 12,
                   treeheight_row = 100) -> OTU_heatmap


```

